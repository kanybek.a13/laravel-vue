<?php

namespace App\Api\V1\Classes;

class Message
{
    public $statusCode;
    public $message;
    public $content;

    /**
     * Message constructor.
     * @param string $message
     * @param null $content
     * @param int $statusCode
     */
    public function __construct($message = "", $content = null, $statusCode = 200)
    {
        $this->statusCode = $statusCode;
        $this->message = $message;
        $this->content = $content;
    }
}
