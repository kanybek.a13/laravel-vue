<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Classes\Message;
use App\Api\V1\Controllers\BaseController;
use App\Api\V1\Transformers\MessageTransformer;
use App\Helpers\PasswordHelper;
use App\Mail\NewPasswordMail;
use App\Mail\PasswordResetCodeMail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AuthController extends BaseController
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
            'password' => 'required',
        ]);

        if ($validator->errors()->count() > 0) {
            $message = new Message(implode(' ', $validator->errors()->all()), null, 400);
            return $this->response->item($message, new MessageTransformer());
        }

        $email = $request->get('email');
        $password = $request->get('password');

        $user = User::whereEmail($email)
            ->first();

        if ($user == null) {
            $message = new Message(__('default.errors.invalid_login'), null, 404);
            return $this->response->item($message, new MessageTransformer());
        }

        if (!Auth::attempt(['email' => $email, 'password' => $password])) {
            $message = new Message(__('default.errors.invalid_credentials'), null, 401);
            return $this->response->item($message, new MessageTransformer());
        }

        $token = $user->createToken('api')->plainTextToken;

        $data = [
            'id' => $user->id,
            'name' => $user->name,
            'role' => $user->roles->pluck('name')->toArray(),
            'token' => $token
        ];

        $message = new Message(__('default.messages.success'), $data);
        return $this->response->item($message, new MessageTransformer());
    }

    public function logout(Request $request)
    {
        $user = $request->user();
        $user->tokens()->delete();

        $message = new Message(__('default.messages.success'));
        return $this->response->item($message, new MessageTransformer());
    }
}
