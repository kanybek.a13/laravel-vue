<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Classes\Message;
use App\Api\V1\Controllers\BaseController;
use App\Api\V1\Transformers\MessageTransformer;
use App\Models\Application;
use App\Models\Template;
use Illuminate\Http\Request;

class DictionaryController extends BaseController
{
    public function applicationStatuses()
    {
        $data = [
            [
                'id' => 0,
                'name' => __('default.application.statuses.0'),
            ],
            [
                'id' => 1,
                'name' => __('default.application.statuses.1'),
            ],
        ];

        $message = new Message(__('default.messages.success'), $data);
        return $this->response->item($message, new MessageTransformer());
    }

    public function templateTypes()
    {
        $data = [
            [
                'id' => 1,
                'name' => __('default.template.types.1'),
            ],
            [
                'id' => 2,
                'name' => __('default.template.types.2'),
            ],
        ];

        $message = new Message(__('default.messages.success'), $data);
        return $this->response->item($message, new MessageTransformer());
    }

    public function templates(Request $request)
    {
        $data = Template::whereDeletedAt(null)
            ->select('id', 'name')
            ->get()
            ->map(function ($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->name
                ];
            })
            ->sortBy('name')
            ->toArray();

        $message = new Message(__('default.messages.success'), array_values($data));
        return $this->response->item($message, new MessageTransformer());
    }
}
