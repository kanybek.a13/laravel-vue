<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Serializers\CustomSerializer;
use App\Helpers\RecordTransformHelper;
use App\Http\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Dingo\Api\Transformer\Adapter\Fractal;
use Illuminate\Support\Facades\App;
use League\Fractal\Manager;
use League\Fractal\TransformerAbstract;

abstract class BaseController extends Controller
{
    use Helpers;

    protected $perPage = 12;

    protected $order;

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        app("Dingo\Api\Transformer\Factory")->setAdapter(function () {
            $fractal = new Manager();
            $fractal->setSerializer(new CustomSerializer());

            return new Fractal($fractal);
        });

        $this->order = 'name_' . App::getLocale();
    }

    /**
     * @param $items
     * @param TransformerAbstract $transformer
     * @param false $extended
     * @return array
     */
    protected function transformItems($items, TransformerAbstract $transformer, $extended = false)
    {
        return RecordTransformHelper::transformItems($items, $transformer, $extended);
    }

    /**
     * @param $item
     * @param TransformerAbstract $transformer
     * @param false $extended
     * @return array
     */
    protected function transformItem($item, TransformerAbstract $transformer, $extended = false, $data = null)
    {
        return RecordTransformHelper::transformItem($item, $transformer, $extended, $data);
    }
}
