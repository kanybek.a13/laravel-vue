<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Classes\Message;
use App\Api\V1\Transformers\ProductTransformer;
use App\Api\V1\Transformers\MessageTransformer;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ProductController extends BaseController
{
    public function index(Request $request)
    {
        $title = $request->get('title');
        $author = $request->get('author');
        $date = $request->get('date');
        $sortBy = $request->get('sortBy', 'dateDesc');

        $query = Product::query();

        if ($title) {
            $query->where('title', 'like', '%' . $title . '%');
        }

        if ($author) {
            $query->whereHas('user', function($query) use ($author) {
                $query->where('name', 'like', '%' . $author . '%');
            });
        }

        if ($date) {
            $query->whereDate('created_at', $date);
        }

        $items = $query->sort($sortBy)
            ->paginate($this->perPage);

        $data = [
            'items' => $this->transformItems($items->items(), new ProductTransformer()),
            'current_page' => $items->currentPage(),
            'total_pages' => $items->lastPage(),
        ];

        $message = new Message(__('default.messages.success'), $data);
        return $this->response->item($message, new MessageTransformer());
    }

    public function view($item)
    {
        $item = Product::whereId($item)
            ->firstOrFail();

        $message = new Message(__('default.messages.success'), $this->transformItem($item, new ProductTransformer(), true));
        return $this->response->item($message, new MessageTransformer());
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required'],
            'description' => ['nullable'],
            'price' => ['required'],
            'user_id' => ['required', 'exists:users,id'],
        ]);

        if ($validator->fails()) {
            throw new HttpException(400, implode(' ', $validator->errors()->all()));
        }

        $title = $request->get('title');
        $description = $request->get('description');
        $userId = $request->get('user_id');
        $price = $request->get('price');

        $item = new Product();
        $item->title = $title;
        $item->description = $description;
        $item->price = $price;
        $item->user_id = $userId;
        $item->save();

        $message = new Message(__('default.messages.success'), $this->transformItem($item, new ProductTransformer(), true));
        return $this->response->item($message, new MessageTransformer());
    }

    public function update(Request $request, $item)
    {
        $item = Product::whereId($item)
            ->firstOrFail();

        $validator = Validator::make($request->all(), [
            'title' => ['required'],
            'description' => ['nullable'],
            'price' => ['required'],
            'user_id' => ['required', 'exists:users,id'],
        ]);

        if ($validator->errors()->count() > 0) {
            throw new HttpException(400, implode(' ', $validator->errors()->all()));
        }

        $title = $request->get('title');
        $description = $request->get('description');
        $userId = $request->get('user_id');
        $price = $request->get('price');

        $item->title = $title;
        $item->description = $description;
        $item->user_id = $userId;
        $item->price = $price;
        $item->save();

        $message = new Message(__('default.messages.success'), $this->transformItem($item, new ProductTransformer(), true));
        return $this->response->item($message, new MessageTransformer());
    }

    public function delete(Request $request, $item)
    {
        $item = Product::whereId($item)
            ->firstOrFail();

        $item->delete();

        $message = new Message(__('default.messages.success'));
        return $this->response->item($message, new MessageTransformer());
    }
}
