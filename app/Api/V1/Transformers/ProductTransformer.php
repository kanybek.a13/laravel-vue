<?php

namespace App\Api\V1\Transformers;

use App\Models\Application;
use App\Models\Product;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{
    public function transform(Product $item)
    {
        return [
            'id' => $item->id,
            'title' => $item->title,
            'description' => $item->description,
            'price' => $item->price,
            'user' => [
                'id' => $item->user->id,
                'name' => $item->user->name
            ],
            'created_date' => Carbon::create($item->created_at)->format('d/m/Y'),
        ];
    }
}
