<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Classes\Message;
use League\Fractal\TransformerAbstract;

class MessageTransformer extends TransformerAbstract
{
    /**
     * @param Message $message
     * @return array
     */
    public function transform(Message $message)
    {
        return [
            "statusCode" => $message->statusCode,
            "message" => $message->message,
            "content" => $message->content,
        ];
    }

}
