<?php

namespace App\Helpers;

use League\Fractal\TransformerAbstract;

class RecordTransformHelper
{
    public static function transformItems($items, TransformerAbstract $transformer, $extended = false)
    {
        $arr = [];
        foreach ($items as $item) {
            $arr[] = $transformer->transform($item, $extended);
        }
        return $arr;
    }

    public static function transformItem($item, TransformerAbstract $transformer, $extended = false, $data = null)
    {
        return $transformer->transform($item, $extended, $data);
    }
}
