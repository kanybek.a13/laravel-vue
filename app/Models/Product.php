<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description', 'user_id', 'price'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function scopeSort($query, $sort)
    {
        if ($sort === 'titleAsc') {
            $query->orderBy('title', 'asc');
        } elseif ($sort === 'titleDesc') {
            $query->orderBy('title', 'desc');
        } elseif ($sort === 'dateAsc') {
            $query->orderBy('created_at', 'asc');
        } else {
            $query->orderBy('created_at', 'desc');
        }
    }
}
