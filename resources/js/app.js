// require('./bootstrap');
//
// window.Vue = require('vue').default;
//
// Vue.component('products-page', require('./components/ProductsComponent.vue').default);
//
// const app = new Vue({
//     el: '#app',
// });


import {createApp} from 'vue'

require('./bootstrap')
import App from './App.vue'
import axios from 'axios'
import router from './router'

const app = createApp(App)
app.config.globalProperties.$axios = axios;
app.use(router)
app.mount('#app')
