<?php

return [
    'messages' => [
        'success' => 'Success',
        'forbidden' => 'Forbidden',

        'auth_failed' => 'Неверный логин или пароль',
    ],

    'roles' => [
        'admin' => 'Администратор',
        'moderator' => 'Модератор',
        'user' => 'Пользователь',
    ],

    'labels' => [
        'email' => 'Почта',
        'password' => 'Пароль',

        'language' => 'language',
    ],

    'errors' => [
        'invalid_credentials' => 'Указан неверный логин и пароль',
        'invalid_login' => 'Нет пользователя с указанным логином',
        'need_auth' => 'Пожалуйста авторизуйтесь',
        'need_permission' => 'У вас недостаточно прав для доступа к выбранному разделу',
        'record_exist' => 'Запись уже создана',
    ],
];
