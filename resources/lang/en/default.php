<?php

return [
    'messages' => [
        'success' => 'Success',
        'forbidden' => 'Forbidden',

        'auth_failed' => 'Invalid username or password',
    ],

    'roles' => [
        'admin' => 'Administrator',
        'moderator' => 'Moderator',
        'user' => 'User',
    ],

    'labels' => [
        'email' => 'Mail',
        'password' => 'Password',

        'language' => 'language',
    ],

    'errors' => [
        'invalid_credentials' => 'Invalid username and password specified',
        'invalid_login' => 'There is no user with the specified username',
        'need_auth' => 'Please log in',
        'need_permission' => 'You do not have enough rights to access the selected section',
        'record_not_found' => 'The specified entry was not found',
    ],
];
