<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => null,
            'title' => $this->faker->title,
            'description' => $this->faker->text,
            'price' => mt_rand(100, 1000),
        ];
    }
}
