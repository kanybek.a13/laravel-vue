<?php

use App\Api\V1\Controllers\Admin\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', ['name' => 'api.', 'namespace' => 'App\Api\V1\Controllers'], function () use (&$api) {
    $api->group(['prefix' => 'auth', 'name' => 'auth.'], function () use (&$api) {
        $api->post('/login', 'AuthController@login')->name('login');

        $api->group(['middleware' => 'auth:sanctum'], function () use (&$api) {
            $api->post('/logout', 'AuthController@logout')->name('logout');
        });
    });

    $api->group(['middleware' => ['bindings', 'auth:sanctum']], function () use (&$api) {
        $api->group(['prefix' => 'dictionaries', 'name' => 'dictionaries.'], function ($api) {
            $api->get('/templates', 'DictionaryController@templates')->name('templates');
        });

        $api->group(['prefix' => 'products', 'name' => 'products.'], function () use (&$api) {
            $api->get('/', 'ProductController@index')->name('index');
            $api->post('/store', 'ProductController@store')->name('store');
            $api->get('/{item}', 'ProductController@view')->name('view');
            $api->post('/{item}', 'ProductController@update')->name('update');
            $api->delete('/{item}', 'ProductController@delete')->name('delete');
        });
    });
});



